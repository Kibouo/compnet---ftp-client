import socket as socket_mod
from string import split
from sys import exit, argv, stdin
from threading import Thread, Lock, ThreadError
from threading import Event as ThreadEvent

SERVER_CTRL_PORT = 21
CMD_BUFF_SIZE = 64
CMD_DELIM = "\r\n"


class FIFOInbox():
    def __init__(self):
        self.__lock = Lock()
        self.__data = []

    def pop(self):
        try:
            self.__lock.acquire()
            value = self.__data[0]
            self.__data = self.__data[1:]
            return value
        finally:
            self.__lock.release()

    def push(self, item):
        if (item != ""):
            try:
                self.__lock.acquire()
                self.__data.append(item)
            finally:
                self.__lock.release()

    def len(self):
        try:
            self.__lock.acquire()
            return len(self.__data)
        finally:
            self.__lock.release()


class DataTransferProcess(Thread):
    # Manages files (send, recv, file system)
    def __init__(self):
        Thread.__init__(self)

    def run(self):
        # Executes on start of thread
        pass


class ProtocolInterpreter(Thread):
    # Interprets & communicates commands with server
    def __init__(self, PI_inbox, CL_inbox, server_hostname, server_port):
        Thread.__init__(self)
        self.__running = True
        # FTP is a "conversation" protocol.
        # We wait for the server, then answer, rinse repeat.
        self.__listening = True
        self.__recv_buff = ""
        self.__send_cmd_buff = PI_inbox
        self.__CL_inbox = CL_inbox

        self.__server_addr = (server_hostname, server_port)

    def __eval_cmd(self, cmd):
        # throws on an error
        code = int(cmd[0])

        # wait, more is comming
        if (code == 1):
            self.__listening = True
        # success
        elif (code == 2):
            self.__listening = False
        # want an extra command
        elif (code == 3):
            self.__listening = False
        # temp error
        elif (code == 4):
            self.__listening = False
            raise Exception(cmd)
        # complete error
        elif (code == 5):
            self.__listening = False
            raise Exception(cmd)

    def __handle_conn(self):
        if (self.__listening):
            try:
                self.__recv_buff = self.__recv_buff + \
                    self.__socket.recv(CMD_BUFF_SIZE)

                split_buff = self.__recv_buff.split(CMD_DELIM)
                cmd = split_buff[0]
                self.__recv_buff = CMD_DELIM.join(split_buff[1:])

                self.__CL_inbox.push(cmd)
                self.__eval_cmd(cmd)
            except socket_mod.timeout:
                pass
            # TODO: recover from errors
            # except Exception:
            #    pass
        else:
            try:
                self.__socket.sendall(self.__send_cmd_buff.pop() + CMD_DELIM)
                self.__listening = True
            except IndexError:
                pass

    def run(self):
        # Executes on start of thread
        try:
            self.__socket = socket_mod.socket()
            self.__socket.settimeout(0.5)
            # TODO: remove debug option
            self.__socket.setsockopt(socket_mod.SOL_SOCKET,
                                     socket_mod.SO_REUSEADDR, 1)
            self.__socket.connect(self.__server_addr)

            while self.__running:
                self.__handle_conn()
        finally:
            self.__socket.close()

    def stop(self):
        self.__running = False


class CommandLine():
    def __init__(self, CL_inbox, PI_inbox):
        self.__to_be_printed = CL_inbox
        self.__PI_inbox = PI_inbox

    def start(self, username, password):
        self.__PI_inbox.push("user " + username)
        self.__PI_inbox.push("pass " + password)

        while True:
            # TODO: fix busy waiting as it is very inefficient
            if (self.__to_be_printed.len() != 0):
                try:
                    print self.__to_be_printed.pop()
                except IndexError:
                    pass

                self.__PI_inbox.push(stdin.readline()[:-1])


def parse_cmd_args():
    try:
        try:
            port = int(argv[1].split(":")[1])
            hostname = argv[1].split(":")[0]
        except IndexError:
            hostname = argv[1]
            port = SERVER_CTRL_PORT

        username = argv[2]
        password = argv[3]

        return (hostname, port, username, password)
    except IndexError:
        exit(1)


def main():
    (server_hostname, server_port, username, password) = parse_cmd_args()
    PI_inbox = FIFOInbox()
    CL_inbox = FIFOInbox()

    PI = ProtocolInterpreter(PI_inbox, CL_inbox, server_hostname, server_port)
    PI.start()

    try:
        CLI = CommandLine(CL_inbox, PI_inbox)
        CLI.start(username, password)
    except KeyboardInterrupt:
        PI.stop()


main()
